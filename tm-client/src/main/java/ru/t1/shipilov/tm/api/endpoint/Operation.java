package ru.t1.shipilov.tm.api.endpoint;

import ru.t1.shipilov.tm.dto.request.AbstractRequest;
import ru.t1.shipilov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
