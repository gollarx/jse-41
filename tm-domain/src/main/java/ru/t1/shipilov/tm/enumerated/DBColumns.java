package ru.t1.shipilov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum DBColumns {

    ID_COLUMN("ID"),
    CREATED_COLUMN("CREATED"),
    USER_ID_COLUMN("USER_ID"),
    NAME_COLUMN("NAME"),
    DESCRIPTION_COLUMN("DESCRIPTION"),
    STATUS_COLUMN("STATUS"),
    PROJECT_ID_COLUMN("PROJECT_ID"),
    ROLE_COLUMN("ROLE"),
    FIRST_NAME_COLUMN("FIRST_NAME"),
    LAST_NAME_COLUMN("LAST_NAME"),
    MIDDLE_NAME_COLUMN("MIDDLE_NAME"),
    EMAIL_COLUMN("EMAIL"),
    LOGIN_COLUMN("LOGIN"),
    PASSWORD_COLUMN("PASSWORD_HASH"),
    SESSION_DATE_COLUMN("SESSION_DATE"),
    LOCKED_COLUMN("LOCKED");

    @NotNull
    private final String columnName;

    DBColumns(@NotNull final String columnName) {
        this.columnName = columnName;
    }

    @NotNull
    public String getColumnName() {
        return columnName;
    }

}
