package ru.t1.shipilov.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.enumerated.Sort;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.dto.model.TaskDTO;

import java.util.*;

public interface TaskConstant {

    int INIT_COUNT_TASKS = 5;

    @NotNull
    String USER_ID_1 = "tst-usr-task-id-1";

    @NotNull
    String USER_ID_2 = "tst-usr-task-id-2";

    @Nullable
    TaskDTO NULLABLE_TASK = null;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_TASK_ID = null;

    @NotNull
    String EMPTY_TASK_ID = "";

    @NotNull
    Sort CREATED_SORT = Sort.BY_CREATED;

    @NotNull
    Sort NAME_SORT = Sort.BY_NAME;

    @NotNull
    Sort STATUS_SORT = Sort.BY_STATUS;

    @Nullable
    Sort NULLABLE_SORT = null;

    @NotNull
    Comparator<TaskDTO> TASK_COMPARATOR = CREATED_SORT.getComparator();

    @Nullable
    Comparator<TaskDTO> NULLABLE_COMPARATOR = null;

    @Nullable
    Integer NULLABLE_INDEX = null;

    @NotNull
    Status IN_PROGRESS_STATUS = Status.IN_PROGRESS;

}
