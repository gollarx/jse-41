package ru.t1.shipilov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.api.service.*;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.exception.entity.EntityNotFoundException;
import ru.t1.shipilov.tm.exception.entity.UserNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.*;
import ru.t1.shipilov.tm.exception.user.ExistsEmailException;
import ru.t1.shipilov.tm.exception.user.ExistsLoginException;
import ru.t1.shipilov.tm.exception.user.RoleEmptyException;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.dto.model.UserDTO;
import ru.t1.shipilov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static ru.t1.shipilov.tm.constant.UserConstant.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull private static IPropertyService propertyService;

    @NotNull
    private static IUserService userService;

    @NotNull
    private List<UserDTO> userList;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
        propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        userService = new UserService(propertyService, connectionService);
    }

    @Before
    public void init() {
        userService.clear();
        userList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_USERS; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("User_" + i);
            user.setEmail("User_" + i + "@test.ru");
            user.setFirstName("User_" + i);
            user.setLastName("Userovov_" + i);
            user.setLastName("Userovich_" + i);
            userService.add(user);
            userList.add(user);
        }
    }

    @After
    public void closeConnection() {
        userService.clear();
    }

    @AfterClass
    public static void defaultUsersRestore() {
        try {
            IPropertyService propertyService = new PropertyService();
            IConnectionService connectionService = new ConnectionService(propertyService);
            IUserService userService = new UserService(propertyService, connectionService);
            userService.create("admin", "admin", Role.ADMIN);
            userService.create("user 1", "1", "first@site.com");
            userService.create("user 2", "2", "second@site.com");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void testClearPositive() {
        Assert.assertEquals(INIT_COUNT_USERS, userService.findAll().size());
        userService.clear();
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void testFindAll() {
        @NotNull List<UserDTO> users = userService.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(userList.size(), users.size());
        for (final UserDTO user : users) {
            Assert.assertEquals(user.getId(), userList.get(users.indexOf(user)).getId());
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testAddUserNegative() {
        userService.add(NULLABLE_USER);
    }

    @Test
    public void testAddUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> userService.add(NULLABLE_USER));
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("tstUsr");
        userService.add(user);
        Assert.assertEquals(INIT_COUNT_USERS + 1, userService.findAll().size());
    }

    @Test
    public void testAdd() {
        @NotNull final UserDTO user = new UserDTO();
        Assert.assertEquals(userList.size(), userService.findAll().size());
        userService.clear();
        userList.clear();
        user.setLogin("tstUsr");
        userService.add(user);
        userList.add(0, user);
        Assert.assertEquals(userList.size(), userService.findAll().size());
        for (int i = 0; i < userList.size(); i++)
            Assert.assertEquals(userList.get(i).getId(), userService.findAll().get(i).getId());
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findById(EMPTY_USER_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        Assert.assertNull(userService.findById(UUID.randomUUID().toString()));
        for (final UserDTO user : userList) {
            Assert.assertEquals(user.getId(), userService.findById(user.getId()).getId());
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveNegative() {
        userService.remove(NULLABLE_USER);
    }

    @Test
    public void testRemove() {
        for (final UserDTO user : userList) {
            userService.remove(user);
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(EMPTY_USER_ID));
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final UserDTO user : userList) {
            userService.removeById(user.getId());
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void testCreateNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(NULLABLE_LOGIN, NULLABLE_PASSWORD));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(EMPTY_LOGIN, NULLABLE_PASSWORD));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create(userList.get(0).getLogin(), NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", EMPTY_PASSWORD));

        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(NULLABLE_LOGIN, NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(EMPTY_LOGIN, NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create(userList.get(0).getLogin(), NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", EMPTY_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(ExistsEmailException.class, () -> userService.create("USR", "USR", userList.get(0).getEmail()));

        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(NULLABLE_LOGIN, NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(EMPTY_LOGIN, NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create(userList.get(0).getLogin(), NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", EMPTY_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create("USR", "USR", NULLABLE_ROLE));
    }

    @Test
    public void testCreatePositive() {
        Assert.assertNotNull(userService.create("USR", "USR", NULLABLE_EMAIL));
        Assert.assertNotNull(userService.create("USR2", "USR2", "EMAIL"));
        Assert.assertNotNull(userService.create("USR3", "USR3", Role.USUAL));
        Assert.assertEquals(INIT_COUNT_USERS + 3, userService.findAll().size());
    }

    @Test
    public void testUpdateUserNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.updateUser(UUID.randomUUID().toString(), null, null, null));
    }

    @Test
    public void testUpdateUserPositive() {
        for (final UserDTO user : userList) {
            Assert.assertNotEquals("FST", user.getFirstName());
            Assert.assertNotEquals("LST", user.getLastName());
            Assert.assertNotEquals("MID", user.getMiddleName());
            userService.updateUser(user.getId(), "FST", "LST", "MID");
            Assert.assertEquals("FST", userService.findById(user.getId()).getFirstName());
            Assert.assertEquals("LST", userService.findById(user.getId()).getLastName());
            Assert.assertEquals("MID", userService.findById(user.getId()).getMiddleName());
        }
    }

    @Test
    public void testSetPasswordNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(NULLABLE_USER_ID, NULLABLE_PASSWORD));
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(EMPTY_USER_ID, NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(UUID.randomUUID().toString(), NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(UUID.randomUUID().toString(), EMPTY_PASSWORD));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.setPassword(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testSetPasswordPositive() {
        for (final UserDTO user : userList) {
            userService.setPassword(user.getId(), "PSW");
            Assert.assertEquals(HashUtil.salt(propertyService, "PSW"), userService.findById(user.getId()).getPasswordHash());
        }
    }

    @Test
    public void testIsLoginExists() {
        Assert.assertFalse(userService.isLoginExist(NULLABLE_LOGIN));
        Assert.assertFalse(userService.isLoginExist(EMPTY_LOGIN));
        Assert.assertTrue(userService.isLoginExist(userList.get(0).getLogin()));
    }

    @Test
    public void testIsEmailExists() {
        Assert.assertFalse(userService.isEmailExist(NULLABLE_EMAIL));
        Assert.assertFalse(userService.isEmailExist(EMPTY_EMAIL));
        Assert.assertTrue(userService.isEmailExist(userList.get(0).getEmail()));
    }

    @Test
    public void testRemoveByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testRemoveByLoginPositive() {
        for (final UserDTO user : userList) {
            userService.removeByLogin(user.getLogin());
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void testRemoveByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(NULLABLE_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(EMPTY_EMAIL));
    }

    @Test
    public void testRemoveByEmailPositive() {
        for (final UserDTO user : userList) {
            userService.removeByEmail(user.getEmail());
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void testFindByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(NULLABLE_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(EMPTY_EMAIL));
    }

    @Test
    public void testFindByEmailPositive() {
        for (final UserDTO user : userList) {
            Assert.assertEquals(user.getId(), userService.findByEmail(user.getEmail()).getId());
        }
    }

    @Test
    public void testFindByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testFindByLoginPositive() {
        for (final UserDTO user : userList) {
            Assert.assertEquals(user.getId(), userService.findByLogin(user.getLogin()).getId());
        }
    }

    @Test
    public void testFindByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findById(EMPTY_USER_ID));
    }

    @Test
    public void testFindByIdPositive() {
        for (final UserDTO user : userList) {
            Assert.assertEquals(user.getId(), userService.findById(user.getId()).getId());
        }
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(EMPTY_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void testLockUserByLoginPositive() {
        for (final UserDTO user : userList) {
            Assert.assertFalse(userService.findByLogin(user.getLogin()).getLocked());
            userService.lockUserByLogin(user.getLogin());
            Assert.assertTrue(userService.findByLogin(user.getLogin()).getLocked());
        }
    }

    @Test
    public void testUnlockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(EMPTY_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void testUnlockUserByLoginPositive() {
        testLockUserByLoginPositive();
        for (final UserDTO user : userList) {
            Assert.assertTrue(userService.findByLogin(user.getLogin()).getLocked());
            userService.unlockUserByLogin(user.getLogin());
            Assert.assertFalse(userService.findByLogin(user.getLogin()).getLocked());
        }
    }

}
