package ru.t1.shipilov.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.api.repository.ITaskRepository;
import ru.t1.shipilov.tm.api.service.IConnectionService;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;
import ru.t1.shipilov.tm.dto.model.TaskDTO;
import ru.t1.shipilov.tm.service.ConnectionService;
import ru.t1.shipilov.tm.service.PropertyService;

import java.util.*;

import static ru.t1.shipilov.tm.constant.TaskConstant.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private ITaskRepository repository;

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private ProjectDTO project;

    @NotNull
    private static SqlSession sqlSession;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
    }

    @Before
    public void init() {
        repository = sqlSession.getMapper(ITaskRepository.class);
        taskList = new ArrayList<>();
        project = new ProjectDTO();
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setProjectId(project.getId());
            repository.add(USER_ID_1, task);
            task.setUserId(USER_ID_1);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            repository.add(USER_ID_2, task);
            task.setUserId(USER_ID_2);
            taskList.add(task);
        }
    }

    @After
    public void clearAfter() {
        repository.clear(USER_ID_1);
        repository.clear(USER_ID_2);
        sqlSession.commit();
    }

    @AfterClass
    public static void closeConnection() {
        sqlSession.close();
    }

    @Test
    public void testAddTaskPositive() {
        TaskDTO task = new TaskDTO();
        task.setName("TaskAddTest");
        task.setDescription("TaskAddTest desc");
        repository.add(USER_ID_1, task);
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        repository.clear(USER_ID_1);
        Assert.assertEquals(0, repository.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        repository.clear(USER_ID_2);
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final TaskDTO task : taskList) {
            final TaskDTO foundTask = repository.findOneById(task.getUserId(), task.getId());
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(task.getId(), foundTask.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final TaskDTO task : taskList) {
            Assert.assertTrue(repository.existsById(task.getUserId(), task.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(USER_ID_1, 9999));
        Assert.assertNull(repository.findOneByIndex(USER_ID_2, 9999));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final TaskDTO foundTask = repository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i).getId(), foundTask.getId());
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final TaskDTO foundTask = repository.findOneByIndex(USER_ID_2, i);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i + 5).getId(), foundTask.getId());
        }
    }

    @Test
    public void testFindAll() {
        @NotNull List<TaskDTO> tasks = repository.findAll(USER_ID_1);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(tasks.get(i).getId(), taskList.get(i).getId());
        }
        tasks = repository.findAll(USER_ID_2);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(tasks.get(i - 5).getId(), taskList.get(i).getId());
        }
    }

    @Test
    public void testFindAllOrderCreated() {
        List<TaskDTO> tasks = repository.findAllOrderCreated(USER_ID_1);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAllOrderCreated(USER_ID_2);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderStatus() {
        List<TaskDTO> tasks = repository.findAllOrderStatus(USER_ID_1);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAllOrderStatus(USER_ID_2);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderName() {
        List<TaskDTO> tasks = repository.findAllOrderName(USER_ID_1);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAllOrderName(USER_ID_2);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final TaskDTO task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            repository.removeById(USER_ID_1, taskList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_1, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            repository.removeById(USER_ID_2, taskList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_2, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            repository.remove(USER_ID_1, taskList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            repository.remove(USER_ID_2, taskList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testTaskFindByProjectId() {
        List<TaskDTO> tasks = repository.findAllByProjectId(USER_ID_1, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
    }

}
