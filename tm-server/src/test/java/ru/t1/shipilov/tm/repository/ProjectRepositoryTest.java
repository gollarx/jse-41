package ru.t1.shipilov.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.api.repository.IProjectRepository;
import ru.t1.shipilov.tm.api.service.IConnectionService;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;
import ru.t1.shipilov.tm.service.ConnectionService;
import ru.t1.shipilov.tm.service.PropertyService;

import java.util.*;

import static ru.t1.shipilov.tm.constant.ProjectConstant.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private IProjectRepository repository;

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static SqlSession sqlSession;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
    }

    @Before
    public void init() {
        repository = sqlSession.getMapper(IProjectRepository.class);
        projectList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            repository.add(USER_ID_1, project);
            project.setUserId(USER_ID_1);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            repository.add(USER_ID_2, project);
            project.setUserId(USER_ID_2);
            projectList.add(project);
        }
    }

    @After
    public void clearAfter() {
        repository.clear(USER_ID_1);
        repository.clear(USER_ID_2);
        sqlSession.commit();
    }

    @AfterClass
    public static void closeConnection() {
        sqlSession.close();
    }

    @Test
    public void testAddProjectPositive() {
        ProjectDTO project = new ProjectDTO();
        project.setName("ProjectAddTest");
        project.setDescription("ProjectAddTest desc");
        repository.add(USER_ID_1, project);
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        repository.clear(USER_ID_1);
        Assert.assertEquals(0, repository.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        repository.clear(USER_ID_2);
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final ProjectDTO project : projectList) {
            final ProjectDTO foundProject = repository.findOneById(project.getUserId(), project.getId());
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(project.getId(), foundProject.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final ProjectDTO project : projectList) {
            Assert.assertTrue(repository.existsById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(USER_ID_1, 9999));
        Assert.assertNull(repository.findOneByIndex(USER_ID_2, 9999));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final ProjectDTO foundProject = repository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i).getId(), foundProject.getId());
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final ProjectDTO foundProject = repository.findOneByIndex(USER_ID_2, i);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i + 5).getId(), foundProject.getId());
        }
    }

    @Test
    public void testFindAll() {
        @NotNull List<ProjectDTO> projects = repository.findAll(USER_ID_1);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertEquals(projects.get(i).getId(), projectList.get(i).getId());
        }
        projects = repository.findAll(USER_ID_2);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projects.get(i - 5).getId(), projectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllOrderCreated() {
        List<ProjectDTO> projects = repository.findAllOrderCreated(USER_ID_1);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAllOrderCreated(USER_ID_2);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderStatus() {
        List<ProjectDTO> projects = repository.findAllOrderStatus(USER_ID_1);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAllOrderStatus(USER_ID_2);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderName() {
        List<ProjectDTO> projects = repository.findAllOrderName(USER_ID_1);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAllOrderName(USER_ID_2);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            repository.removeById(USER_ID_1, projectList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_1, projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            repository.removeById(USER_ID_2, projectList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_ID_2, projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            repository.remove(USER_ID_1, projectList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            repository.remove(USER_ID_2, projectList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

}
