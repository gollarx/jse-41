package ru.t1.shipilov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.api.repository.IProjectRepository;
import ru.t1.shipilov.tm.api.repository.ITaskRepository;
import ru.t1.shipilov.tm.api.service.*;
import ru.t1.shipilov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shipilov.tm.exception.entity.TaskNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.ProjectIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.TaskIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.UserIdEmptyException;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;
import ru.t1.shipilov.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.EMPTY_PROJECT_ID;
import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.EMPTY_TASK_ID;
import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.EMPTY_USER_ID;
import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.INIT_COUNT_TASKS;
import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.NULLABLE_PROJECT_ID;
import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.NULLABLE_TASK_ID;
import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.NULLABLE_USER_ID;
import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.USER_ID_1;
import static ru.t1.shipilov.tm.constant.ProjectTaskConstant.USER_ID_2;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private static IProjectRepository projectRepository;

    @NotNull
    private static ITaskRepository taskRepository;

    @NotNull
    private static SqlSession sqlSession;

    @NotNull
    private static IProjectTaskService projectTaskService;

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private List<TaskDTO> taskList;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        projectTaskService = new ProjectTaskService(connectionService);
        sqlSession = connectionService.getSqlSession();
        projectRepository = sqlSession.getMapper(IProjectRepository.class);
        taskRepository = sqlSession.getMapper(ITaskRepository.class);
    }

    @Before
    public void init() {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project");
            project.setDescription("Description");
            project.setUserId(i == 1 ? USER_ID_1 : USER_ID_2);
            projectRepository.add(project.getUserId(), project);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_1);
            taskRepository.add(USER_ID_1, task);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_2);
            taskRepository.add(USER_ID_2, task);
            taskList.add(task);
        }
        sqlSession.commit();
    }

    @After
    public void closeConnection() {
        sqlSession.clearCache();
        taskRepository.clear(USER_ID_1);
        taskRepository.clear(USER_ID_2);
        projectRepository.clear(USER_ID_1);
        projectRepository.clear(USER_ID_2);
        sqlSession.commit();
    }

    @Test
    public void testBindTaskToProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 0).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void testBindTaskToProjectPositive() {
        for (final ProjectDTO project : projectList) {
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final TaskDTO task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    projectTaskService.bindTaskToProject(project.getUserId(), project.getId(), task.getId());
            }
            sqlSession.clearCache();
            List<TaskDTO> tasks = taskRepository.findAll(project.getUserId());
            for (final TaskDTO task : tasks) {
                Assert.assertNotNull(
                        taskRepository.findAllByProjectId(project.getUserId(), project.getId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testUnbindTaskFromProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 0).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void TestUnbindTaskFromProjectPositive() {
        testBindTaskToProjectPositive();
        for (final ProjectDTO project : projectList) {
            Assert.assertNotEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final TaskDTO task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    projectTaskService.unbindTaskFromProject(project.getUserId(), project.getId(), task.getId());
            }
            sqlSession.clearCache();
            List<TaskDTO> tasks = taskRepository.findAll(project.getUserId());
            for (final TaskDTO task : tasks) {
                Assert.assertNull(
                        taskRepository.findAllByProjectId(project.getUserId(), project.getId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testRemoveProjectByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, EMPTY_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testRemoveProjectByIdPositive() {
        testBindTaskToProjectPositive();
        for (final ProjectDTO project : projectList) {
            Assert.assertNotNull(taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            Assert.assertEquals(INIT_COUNT_TASKS, taskRepository.findAll(project.getUserId()).size());
            projectTaskService.removeProjectById(project.getUserId(), project.getId());
            sqlSession.clearCache();
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            Assert.assertEquals(0, taskRepository.findAll(project.getUserId()).size());
        }
        Assert.assertEquals(0, taskRepository.getSize(USER_ID_1) + taskRepository.getSize(USER_ID_2));
    }

}
