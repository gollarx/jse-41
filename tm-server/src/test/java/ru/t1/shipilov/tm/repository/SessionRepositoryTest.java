package ru.t1.shipilov.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.api.repository.ISessionRepository;
import ru.t1.shipilov.tm.api.service.IConnectionService;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.dto.model.SessionDTO;
import ru.t1.shipilov.tm.service.ConnectionService;
import ru.t1.shipilov.tm.service.PropertyService;

import java.util.*;

import static ru.t1.shipilov.tm.constant.SessionConstant.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private ISessionRepository repository;

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    private List<String> userIdList = new ArrayList<>();

    @NotNull
    private static SqlSession sqlSession;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
    }

    @Before
    public void init() {
        repository = sqlSession.getMapper(ISessionRepository.class);
        sessionList = new ArrayList<>();
        userIdList = new ArrayList<>();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++)
            userIdList.add(UUID.randomUUID().toString());
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final SessionDTO session = new SessionDTO();
            session.setUserId(userIdList.get(i));
            session.setRole(Role.USUAL);
            repository.add(userIdList.get(i), session);
            sessionList.add(session);
        }
    }

    @After
    public void ClearAfter() {
        for (@NotNull final String userId : userIdList) {
            repository.clear(userId);
        }
        sqlSession.commit();
    }

    @AfterClass
    public static void closeConnection() {
        sqlSession.close();
    }

    @Test
    public void testAddSessionPositive() {
        @NotNull SessionDTO session = new SessionDTO();
        session.setUserId(UUID.randomUUID().toString());
        session.setRole(Role.USUAL);
        repository.add(userIdList.get(0), session);
        Assert.assertEquals(2, repository.getSize(userIdList.get(0)));
    }

    @Test
    public void testClear() {
        for (@NotNull final String userId : userIdList) {
            Assert.assertEquals(1, repository.getSize(userId));
            repository.clear(userId);
            Assert.assertEquals(0, repository.getSize(userId));
        }
    }

    @Test
    public void testFindById() {
        for (@NotNull final String userId : userIdList) {
            Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
        }
        for (@NotNull final SessionDTO session : sessionList) {
            final SessionDTO foundSession = repository.findOneById(session.getUserId(), session.getId());
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final SessionDTO session : sessionList) {
            Assert.assertTrue(repository.existsById(session.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        for (@NotNull final SessionDTO session : sessionList) {
            Assert.assertNull(repository.findOneByIndex(session.getUserId(), 9999));
            final SessionDTO foundSession = repository.findOneByIndex(session.getUserId(), 0);
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
    }

    @Test
    public void testFindAll() {
        for (@NotNull final String userId : userIdList) {
            List<SessionDTO> sessions = repository.findAll(userId);
            Assert.assertNotNull(sessions);
            Assert.assertEquals(1, sessions.size());
            for (@NotNull final SessionDTO session : sessionList) {
                if (session.getUserId().equals(userId))
                    Assert.assertNotNull(
                            sessions.stream()
                                    .filter(m -> session.getId().equals(m.getId()))
                                    .filter(m -> session.getUserId().equals(m.getUserId()))
                                    .findFirst()
                                    .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final SessionDTO session : sessionList) {
            repository.removeById(session.getUserId(), session.getId());
            Assert.assertNull(repository.findOneById(session.getUserId(), session.getId()));
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test
    public void testRemove() {
        for (final SessionDTO session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUserId()));
            repository.removeWithUserId(session.getUserId(), session);
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test
    public void testRemoveWOUserId() {
        for (final SessionDTO session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUserId()));
            repository.remove(session);
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

}
