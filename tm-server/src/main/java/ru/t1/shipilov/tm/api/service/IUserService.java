package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserService {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email) ;

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role) ;

    void clear();

    void add(@Nullable UserDTO user);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String Login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    void remove(@Nullable UserDTO model);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    void updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
