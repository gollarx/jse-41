package ru.t1.shipilov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, session_date, user_id, role)" +
            " VALUES (#{session.id}, #{session.date}, #{userId}, #{session.role})")
    void add(@NotNull @Param("userId") String userId, @NotNull @Param("session") SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT id, session_date, user_id, role FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "session_date")
    })
    @NotNull List<SessionDTO> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_session WHERE id = #{id})")
    boolean existsById(@NotNull @Param("id") String id);

    @Select("SELECT id, session_date, user_id, role FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "session_date")
    })
    @Nullable SessionDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT id, session_date, user_id, role FROM tm_session WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "session_date")
    })
    @Nullable SessionDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT count(id) FROM tm_session WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{session.id}")
    void removeWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("session") SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE id = #{session.id}")
    void remove(@NotNull @Param("session") SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

}
