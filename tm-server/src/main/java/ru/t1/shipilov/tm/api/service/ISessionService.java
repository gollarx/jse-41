package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionService {

    void clear(@Nullable final String userId);

    @NotNull
    List<SessionDTO> findAll(@Nullable final String userId);

    boolean existsById(@Nullable final String id);

    @Nullable
    SessionDTO findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    SessionDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    void remove(@NotNull SessionDTO session);

    void add(@Nullable String userId, @Nullable SessionDTO session);

    SessionDTO add(@NotNull SessionDTO session);

}
