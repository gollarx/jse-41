package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.enumerated.Sort;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskService {

    void clear(@Nullable final String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    TaskDTO findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    @NotNull
    List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sort);

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void add(@Nullable String userId, @Nullable TaskDTO task);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}
