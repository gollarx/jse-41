package ru.t1.shipilov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password_hash, email, first_name, last_name, middle_name, locked, role)" +
            " VALUES (#{user.id}, #{user.login}, #{user.passwordHash}, #{user.email}, #{user.firstName}," +
            " #{user.lastName}, #{user.middleName}, #{user.locked}, #{user.role})")
    void add(@NotNull @Param("user") UserDTO user);

    @Delete("TRUNCATE TABLE tm_user")
    void clear();

    @Select("SELECT EXISTS(SELECT 1 FROM tm_user WHERE id = #{id})")
    boolean existsById(@NotNull @Param("id") String id);

    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, locked, role FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @NotNull List<UserDTO> findAll();

    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, locked, role FROM tm_user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findOneById(@NotNull @Param("id") String id);

    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, locked, role FROM tm_user LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findOneByIndex(@NotNull @Param("index") Integer index);

    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, locked, role FROM tm_user WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findByLogin(@NotNull @Param("login") String login);

    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, locked, role FROM tm_user WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findByEmail(@NotNull @Param("email") String email);

    @Select("SELECT count(id) FROM tm_user")
    int getSize();

    @Delete("DELETE FROM tm_user WHERE id = #{user.id}")
    void remove(@NotNull @Param("user") UserDTO user);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_user WHERE login = #{login})")
    @NotNull Boolean isLoginExist(@NotNull @Param("login") String login);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_user WHERE email = #{email})")
    @NotNull Boolean isEmailExist(@NotNull @Param("email") String email);

    @Update("UPDATE tm_user SET login = #{user.login}, password_hash = #{user.passwordHash}, email = #{user.email}," +
            " first_name = #{user.firstName}, last_name = #{user.lastName}, middle_name = #{user.middleName}," +
            " locked = #{user.locked}, role = #{user.role} WHERE id = #{user.id}")
    void update(@NotNull @Param("user") UserDTO user);

}
